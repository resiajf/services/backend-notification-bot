import logging
import re

translations = {
    'en': {
        'start':
        'To complete the activation process of the notification system, send' +
        'the command `/activa ACTIVATION_CODE` with your activation code in ' +
        'the command.\n' +
        '\n' +
        'To see the notifications from the residence\'s news and announcemen' +
        'ts blog, go to the channel [noti_resi_ajf](https://t.me/noti_resi_a' +
        'jf).',
        'invalid_code': 'Invalid format for the activation code: `{}`',
        'last_activate_step': 'Code verified!\nRemember to click the button o' +
            'n the web to finish the process :)',
        'code_not_found': 'This activation code has expired or is invalid.\n' +
            'Try to regenerate a new code in the web page by pressing the ch' +
            'eck button.',
        'code_already_used': 'This activation code has already been used',
        'checking': 'Checking...',
    },
    'es': {
        'start':
        'Para completar el proceso de activación del sistema de notificacion' +
        'es, envia el comando `/activa CODIGO_ACTIVACION` poniendo el código' +
        ' de activación en el comando.\n' +
        '\n' +
        'Para ver las notificaciones del blog de noticias y anuncios de la r' +
        'esidencia, ve al canal [noti_resi_ajf](https://t.me/noti_resi_ajf).',
        'invalid_code': 'Formato inválido de código de activación: `{}`',
        'last_activate_step': '¡Codigo verificado!\nRecuerda apretar el botón' +
            ' la web para acabar el proceso :)',
        'code_not_found': 'Este código de activación ha expirado o es inválid' +
            'o.\nPrueba a regenerar un nuevo código desde la web apretando el' +
            ' botón de comprobar.',
        'code_already_used': 'Este código de activación ya se ha usado',
        'checking': 'Comprobando...',
    }
}


def get(update, key):
    code = update.message.from_user.language_code
    part = None
    if code is not None:
        part = re.compile(r'(\w{2,})-?\w*').match(code).group(1)

        if part in translations:
            if key in translations[part]:
                return translations[part][key]

    logging.getLogger(__name__).warn(f'Untranslated: {code} -> {part} - {key}')
    return translations['es'][key] if key in translations['es'] else key
