import os
import re

import redis
from telegram import ParseMode
from strings import get


r = redis.StrictRedis(
    host=os.environ.get('DB_HOST', 'localhost'),
    port=int(os.environ.get('DB_PORT', 6379)),
    db=int(os.environ.get('DB_NUM', 0)),
    decode_responses=True,
)


activate_command_regex = re.compile(r'/activa ([a-z0-9]{8})', re.IGNORECASE)


def start(bot, update):
    text = get(update, 'start')

    update.message.reply_text(
        (f'Hola {update.message.from_user.first_name}.\n\n{text}'),
        parse_mode=ParseMode.MARKDOWN
    )


def activa(bot, update):
    res = activate_command_regex.match(update.message.text)
    if res == None:
        update.message.reply_text(
            get(update, 'invalid_code').format(update.message.text[8:]),
            reply_to_message_id=update.message.message_id,
            parse_mode=ParseMode.MARKDOWN,
        )
        return

    code = res.group(1)
    user_id = update.effective_user.id
    mid = update.message.reply_text(
        get(update, 'checking'),
        reply_to_message_id=update.message.message_id
    )

    if r.exists(f'telegram:{code}'):
        if r.get(f'telegram:{code}') == '-1':
            r.set(f'telegram:{code}', user_id, ex=60, xx=True)
            mid.edit_text(get(update, 'last_activate_step'))
        else:
            mid.edit_text(get(update, 'code_already_used'))
    else:
        mid.edit_text(get(update, 'code_not_found'))

