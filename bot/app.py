import logging
import os

from telegram.ext import Updater, CommandHandler

from commands import activa, start

if __name__ == '__main__':
    if 'TELEGRAM_BOT_TOKEN' not in os.environ or len(os.environ['TELEGRAM_BOT_TOKEN'] == 0:
        print('Environment variable `TELEGRAM_BOT_TOKEN` not defined')
        os.exit(1)

    logging.getLogger().addHandler(logging.StreamHandler())
    updater = Updater(os.environ['TELEGRAM_BOT_TOKEN'])

    updater.dispatcher.add_handler(CommandHandler('start', start))
    updater.dispatcher.add_handler(CommandHandler('activa', activa))

    updater.start_polling()
    updater.idle()
