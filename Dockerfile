FROM python:3.6-slim

WORKDIR /app
COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY bot ./bot

ENV DB_HOST localhost
ENV DB_PORT 6379
ENV DB_NUM  0

CMD ["python", "bot/app.py"]
